#include "Level3.h"
#include "Global.h"
#include "Box.h"
Level3::Level3()
{
	LoadResources();
	//Islockcamera = false;
}

void Level3::RenderFrame(int Delta)
{
	if (G_lpDirect3DDevice->BeginScene())
	{
		G_lpDirect3DDevice->ColorFill(G_BackBuffer, NULL, D3DCOLOR_XRGB(0, 0, 0));



		G_SpriteHandler->Begin(D3DXSPRITE_ALPHABLEND);
		//camera->SetTransForm();

		objManager->Draw();

		char str[100];
		//blackBoard->RenderFrame(Delta, camera->GetRectCamera().left, camera->GetRectCamera().top, simon->GetHP());
		G_SpriteHandler->End();

		G_lpDirect3DDevice->EndScene();
	}
	G_lpDirect3DDevice->Present(NULL, NULL, NULL, NULL);
}

void Level3::ProcessInput_UP(int Delta)
{
}

void Level3::ProcessInput_DOWN(int Delta)
{
}

void Level3::ProcessInput_RIGHT(int Delta)
{
}

void Level3::ProcessInput_LEFT(int Delta)
{
}

void Level3::ProcessInput(int Delta)
{
}

void Level3::UpdateGame(int Delta)
{
	KeyBoard::GetKey()->ProcessKeyBoard();
	map->SelectScene(camera->GetRectCamera());
	RECT r = RECT();
	Box box = Box::ConvertRECT(r);
	objManager->UpDateTest(camera->GetRectCamera(), Delta, box);// Item::GetStaticObj()->GetListItem(), Delta, simon->GetBox());
	if (NextLevel)
	{
		NextLevel = false;
		IsEnd = true;
	}
}

void Level3::LoadResources()
{
	//
	Scene_Index = 1;
	map = new Map();
	objManager = new ObjManager();
	objManager->Init(nullptr, nullptr);//"Data/lv2-GameObj.txt", "Data/lv2-GameObj-Quadtree.txt");

	//
	KeyBoard::GetKey()->InitKeyboard();
	//GTexture* texture = new GTexture();
	//texture->loadTextTureFromFile(L"caveman.bmp", D3DCOLOR_XRGB(255, 0, 255));

	camera = new GCamera(G_ScreenWidth, G_ScreenHeight, 0, D3DXVECTOR3(1.0f, 1.0f, 1.0f), lv2_Layer3.right * 2 - 200, lv2_Layer3.bottom * 2 - 200, 3);
	
	Font::GetFont()->Innit();
	
	
}

void Level3::OnKeyDown(int KeyCode)
{
}



Level3::~Level3()
{
}
