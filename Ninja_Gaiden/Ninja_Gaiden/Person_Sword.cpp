#include "Person_Sword.h"



Person_Sword::Person_Sword()
{
}

Person_Sword::Person_Sword(int _id, int _type, int _x, int _y, int _Width, int _Height)
{
	index = 0;
	temp = 0;
	x = _x;
	y = _y;
	Type = _type;
	id = _id;
	vecX = 4;
	vecY = 0;
	LoadResource(L"Resources/enemy/person_sword.png", 4, 1, 500);
	this->Sprite->SetAnimation(0, 2);
}

void Person_Sword::Draw()
{
	//Sprite->SelectIndex(index);
	Sprite->Draw(x, y);
}

void Person_Sword::Update(Box RectCamera, Box simon, int Deltatime)
{
	this->Sprite->Update(Deltatime);
}

bool Person_Sword::CheckCollision(Box simon)
{

	return false;
}


Person_Sword::~Person_Sword()
{
}
