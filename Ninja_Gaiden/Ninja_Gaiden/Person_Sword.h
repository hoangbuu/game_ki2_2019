#pragma once
#include "BaseObject.h"
class Person_Sword : BaseObject
{
public:
	Person_Sword();
	Person_Sword(int _id, int _type, int _x, int _y, int _Width, int _Height);
	void Update(Box RectCamera, Box simon, int Deltatime);
	bool CheckCollision(Box simon);
	void Draw();
	~Person_Sword();
};