#ifndef _OBJMANAGER_H_
#define _OBJMANAGER_H_

#include "QuadTree.h"
#include "BaseObject.h"
#include <vector>
#include <d3d9.h>
#include <list>
#include <string>
#include <fstream>
#include <iostream>
#include "Font.h"
#include <stdio.h>
#include <d3d9.h>
#include "Collision.h"
#include "Person_Sword.h"
#include "Ninja.h"

using namespace std;


class ObjManager
{
private:
	vector<int*> InfoObj; //
	vector<int*> InfoObjTemp;
	QuadTree* quadTree;
	list<int> listID;
	vector<BaseObject*> listObj;
	//vector<Enemy*> listEnemy;
	int idsize = 0;
	RECT rect;
	ifstream f;
	Person_Sword* person_Sword;
	Ninja* ninja;
public:
	bool Isclear;
	int Frozen;
	ObjManager()
	{
		quadTree = new QuadTree();
		Isclear = false;
		Frozen = 120;
	}

	bool IsNumber(char c)
	{
		if (c >= '0' && c <= '9')
			return true;
		return false;
	}

	void LoadFileInfo(char* fileGameObj)
	{

		f.open(fileGameObj, ios::in);
		string line;
		string strTemp = "";
		int i = 0;
		int *lTemp;

		while (!f.eof())
		{
			i = 0;
			lTemp = new int[7];
			strTemp = "";
			getline(f, line);
			if (line == "")
				break;
			for (int n = 0; n < line.length(); n++)
			{
				if (line[n] != '\t' && IsNumber(line[n]))
					strTemp += line[n];
				else
				{
					if (IsNumber(strTemp[0]))
					{
						int gt = atoi(strTemp.c_str());
						lTemp[i] = gt;
						strTemp = "";
						i++;
					}
				}
				if (n == line.length() - 1)
					lTemp[i] = atoi(strTemp.c_str());
			}
			lTemp[6] = 0;
			//add info object	
			InfoObj.push_back(lTemp);
		}
	}

	void Init(char* fileGameObj, char* fileQuadTree)
	{
		/*LoadFileInfo(fileGameObj);
		quadTree->GetQuadTreeFromFile(fileQuadTree);
		int* t;
		for (int i = 0; i < InfoObj.size(); i++)
		{
			t = new int[7];
			for (int j = 0; j < 7; j++)
			{
				t[j] = InfoObj[i][j];
			}
			InfoObjTemp.push_back(t);
		}*/
		person_Sword = new Person_Sword(1, 100, 100, 100, 80, 80);
		ninja = new Ninja(1, 100, 150, 100, 80, 80);
	}

	void GetListIDFromquadTree(RECT rectCamera)
	{
		rect = rectCamera;
		listID.clear();
		quadTree->GetlistObj(quadTree->Root, rectCamera, listID);
		idsize = listID.size();

	}

	void AddListAnemy(vector<int> tempEnemy, int* t)
	{
		for (int i = 0; i < tempEnemy.size(); i++)
		{
			/*	Type _type;
				t = InfoObj[tempEnemy[i]];
				_type = (Type)t[1];
				Enemy* obj;
				switch (_type)
				{
				case GroundMovingPlatform:
					obj = new MovePlatform(t[0], t[1], t[2], t[3], t[4], t[5]);
					break;*/

					/*	default:
							break;
						}

						listEnemy.push_back(obj);*/
		}
	}

	void GetListObjFromQuadTree()
	{
		//get list obj game
		//bien phu
		vector<int> tempEnemy;
		std::list<int>::iterator it;
		int *t = new int[7];
		int j;
		Type _type;
		for (int i = 0; i < listObj.size(); i++)
		{
			delete listObj[i];
		}
		listObj.clear();
		for (it = listID.begin(); it != listID.end(); it++)
		{
			t = InfoObj[*it];
			RECT check{ t[2],t[3], t[2] + t[4],t[3] + t[5] };
			if (Collision::CheckCollison(rect, check))
			{
				if (t[1] >= 600 && t[1] != 615 && t[1] != 611)
				{
					BaseObject* obj = new BaseObject(t[0], t[1], t[2], t[3], t[4], t[5]);
					listObj.push_back(obj);
				}
				else
				{
					//Load id enemy
					tempEnemy.push_back(*it);
				}
			}
		}

		//loai bo pt trung
		if (!tempEnemy.empty())
			for (int i = 0; i < tempEnemy.size() - 1; i++)
			{
				for (int j = i + 1; j < tempEnemy.size(); j++)
				{
					if (tempEnemy[i] == tempEnemy[j])
					{
						tempEnemy.erase(tempEnemy.begin() + j);
						j--;
					}

				}
				//if (!tempEnemy.empty())

			}



	}

	void UpDate(RECT rectCamera, vector<Object> &listItem, int DeltaTime, Box simon)
	{
		GetListIDFromquadTree(rectCamera);
		GetListObjFromQuadTree();
		if (Frozen < 120)
		{
			Isclear = true;
			Frozen++;
		}
		if (Isclear)
		{

		}
		this->person_Sword->Update(Box::ConvertRECT(rect), simon, DeltaTime);
		this->ninja->Update(Box::ConvertRECT(rect), simon, DeltaTime);
	}
	void UpDateTest(RECT rectCamera, int DeltaTime, Box simon)
	{
		this->person_Sword->Update(Box::ConvertRECT(rect), simon, DeltaTime);
	}

	void Draw()
	{
		this->person_Sword->Draw();
		this->ninja->Draw();
	}

	vector<BaseObject*> GetListObj()
	{
		return listObj;
	}

	vector<int*> GetListInfo();

	void HandlingCollisionWithGround(vector<Object> &listItem, int DeltaTime)
	{

	}
	void Reset()
	{
		InfoObj.clear();
		int* t;
		for (int i = 0; i < InfoObjTemp.size(); i++)
		{
			t = new int[7];
			for (int j = 0; j < 7; j++)
			{
				t[j] = InfoObjTemp[i][j];
			}
			InfoObj.push_back(t);
		}
	}
	void Delete()
	{
		/*for (int i = 0; i < listEnemy.size(); i++)
		{
			listEnemy[i]->IsDie = true;
			Effect::GetStaticObj()->Add(listEnemy[i]);
		}*/
	}
	~ObjManager()
	{
		//if (quadTree != NULL) delete quadTree; 
		f.close();
	}
};
#endif // !_OBJMANAGER_H_




